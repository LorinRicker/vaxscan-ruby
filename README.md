# vaxscan-ruby : A VAX SCAN Emulator in Ruby

Started: 9-January-2018

Last update: 10-January-2018

## Overview

This project will develop an execution processor for the VAX SCAN programming language, implemented in Ruby.

The idea (goal) is to develop a Ruby application which can consume VAX SCAN source code, including `MACRO [ TRIGGER | SYNTAX ]`, `TOKEN`, `SET` and the activator `SCAN` declarations and statements, and internally generated a translation of these constructs into Ruby objects, including RegExps, which will execute the original SCAN text processing (algorithm).

### TO-DO:

* Find a copy of the VAX SCAN Programmer Manual, scan to PDF (if necessary), put this in coredocs/ too.

* Evaluation and preliminary design.

* Text-processing feature mapping between SCAN and Ruby.

#### Done:

* Get a copy of The Digital Journal (vol 1 nr 6, p 40) article on VAX SCAN for coredocs/
